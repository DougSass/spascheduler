//
//  ViewEmployeesTVC.swift
//  SpaScheduler
//
//  Created by Douglas Sass on 4/17/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit
import Parse

class ViewEmployeesTVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        let addEmployeeBtn = UIBarButtonItem(title: "Add Employee", style: .plain, target: self, action: #selector(self.addEmployeeTouched))
        self.navigationItem.rightBarButtonItem = addEmployeeBtn
    }

    // Go to CreateEmployee vc
    func addEmployeeTouched() -> Void
    {
        let vc = CreateEmployeeVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1 // Change to the total number of employees
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 10
    }
    
    //Add Function for when touching the cell it will take you to the edit employee
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        
        let query = PFQuery(className:"employee")
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
        }
        
        return cell!
    }

}
