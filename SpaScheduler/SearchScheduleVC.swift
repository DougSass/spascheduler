//
//  SearchScheduleVC.swift
//  SpaScheduler
//
//  Created by Douglas Sass on 4/17/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit
import Parse

class SearchScheduleVC: UIViewController {

    var searchScheduleLbl = UILabel()
    
    var employeeName = UITextField()
    var employeeNameLbl = UILabel()
    
    var appointmentDatePicker = UIDatePicker()
    var appointmentDateLbl = UILabel()
    
    var appointmentTimeLbl = UILabel()
    
    var searchScheduleBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.white
        
        //Label for Search Appointment Title
        searchScheduleLbl = UILabel(frame: CGRect(x: 10, y: 50, width: self.view.frame.size.width - 20, height: 45))
        searchScheduleLbl.text = "Search Schedule"
        searchScheduleLbl.textAlignment = NSTextAlignment.center
        searchScheduleLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(searchScheduleLbl)
        
        //Text field for the guest name for the search
        employeeName = UITextField(frame: CGRect(x: 10, y: 120, width: self.view.frame.size.width - 20, height: 45))
        employeeName.placeholder = "Employee Name"
        employeeName.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(employeeName)
        
        //Label for Guest Name
        employeeNameLbl = UILabel(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        employeeNameLbl.text = "Enter Employee's Name:"
        employeeNameLbl.textAlignment = NSTextAlignment.center
        employeeNameLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(employeeNameLbl)
        
        // Pick date for the search
        appointmentDatePicker = UIDatePicker(frame: CGRect(x: 10, y: 210, width: self.view.frame.size.width - 20, height: 45))
        self.view.addSubview(appointmentDatePicker)
        
        // Label for the Date
        appointmentDateLbl = UILabel(frame: CGRect(x: 10, y: 160, width: self.view.frame.size.width - 20, height: 45))
        appointmentDateLbl.text = "Date of Schedule:"
        appointmentDateLbl.textAlignment = NSTextAlignment.center
        appointmentDateLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentDateLbl)
        
        // Button for searching for the Schedule
        let searchScheduleBtn = UIButton(type: UIButtonType.system)
        searchScheduleBtn.frame = CGRect(x: 10, y: 300, width: self.view.frame.size.width - 20, height: 45)
        searchScheduleBtn.setTitle("Search", for: UIControlState.normal)
        searchScheduleBtn.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(searchScheduleBtn)
        searchScheduleBtn.addTarget(self, action: #selector(searchScheduleBtnTouched(btn:)), for: UIControlEvents.touchUpInside)
        
    }
    
    func searchScheduleBtnTouched(btn:UIButton) -> Void
    {
        // Need to verify data entered is valid,
        // Then verify their is an appointment for that date
        //When both are valid, push to view the appointment
        
        // Push to the View Appointment view
        let vc = ViewScheduleVC()
        vc.passedEmployee = employeeName.text!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy"
        let newDate = dateFormatter.string(from: appointmentDatePicker.date)
        vc.passedDate = newDate
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
