//
//  AddAppointmentVC.swift
//  SpaScheduler
//
//  Created by Douglas Sass on 4/15/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit
import Parse

class AddAppointmentVC: UIViewController {
    
    var addAppointmentLbl = UILabel()
    
    var guestName = UITextField()
    var guestNameLbl = UILabel()
    
    var appointmentDatePicker = UIDatePicker()
    var appointmentDateLbl = UILabel()
    
    var appointmentTimePicker = UIDatePicker()
    var appointmentTimeLbl = UILabel()
    var appointmentTimeErrorLbl = UILabel()
    
    var appointmentLength = UITextField()
    var appointmentLengthLbl = UILabel()
    var appointmentLengthErrorLbl = UILabel()
    
    var appointmentRequest = UITextField()
    var appointmentRequestLbl = UILabel()
    var appointmentRequestErrorLbl = UILabel()
    
    var addAppointmentBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.white
        
        //Label for Add Appointment Title
        addAppointmentLbl = UILabel(frame: CGRect(x: 10, y: 50, width: self.view.frame.size.width - 20, height: 45))
        addAppointmentLbl.text = "Add an Appointment"
        addAppointmentLbl.textAlignment = NSTextAlignment.center
        addAppointmentLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(addAppointmentLbl)
        
        //Text field for the Guest's name for the appointment
        guestName = UITextField(frame: CGRect(x: 190, y: 90, width: 70, height: 45))
        guestName.placeholder = "GuestName"
        guestName.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(guestName)
        
        // Label for the Guest's Name for the appointment
        guestNameLbl = UILabel(frame: CGRect(x: 10, y: 90, width: 180, height: 45))
        guestNameLbl.text = "Enter Guest's Name:"
        guestNameLbl.textAlignment = NSTextAlignment.left
        //guestNameLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(guestNameLbl)
        
        // Date picker for the appointment
        appointmentDatePicker = UIDatePicker(frame: CGRect(x: 10, y: 180, width: self.view.frame.size.width, height: 45))
        appointmentDatePicker.datePickerMode = UIDatePickerMode.date
        appointmentDatePicker.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentDatePicker)
        
        // Label for Date of appointment
        appointmentDateLbl = UILabel(frame: CGRect(x: 10, y: 150, width: self.view.frame.size.width - 20, height: 45))
        appointmentDateLbl.text = "Date of Appointment"
        appointmentDateLbl.textAlignment = NSTextAlignment.center
        appointmentDateLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentDateLbl)
        
        //Text field for the Time of the appointment
        appointmentTimePicker = UIDatePicker(frame: CGRect(x: 10, y: 250, width: self.view.frame.size.width, height: 45))
        appointmentTimePicker.datePickerMode = UIDatePickerMode.time
        appointmentTimePicker.minuteInterval = 30
        appointmentTimePicker.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentTimePicker)
        
        //Label for Time
        appointmentTimeLbl = UILabel(frame: CGRect(x: 10, y: 210, width: self.view.frame.size.width - 20, height: 45))
        appointmentTimeLbl.text = "Time of Appointment"
        appointmentTimeLbl.textAlignment = NSTextAlignment.center
        appointmentTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentTimeLbl)

        
        //Decide if it is 30, 60, or 90 minute appointment Length
        appointmentLength = UITextField(frame: CGRect(x: 160, y: 310, width: 180, height: 45))
        appointmentLength.placeholder = "Enter length as 30, 60, or 90"
        appointmentLength.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentLength)
        
        //Label for Length
        appointmentLengthLbl = UILabel(frame: CGRect(x: 10, y: 310, width: 140, height: 45))
        appointmentLengthLbl.text = "Length of Appointment:"
        appointmentLengthLbl.textAlignment = NSTextAlignment.left
        appointmentLengthLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentLengthLbl)
        
        //Label for Length Error
        appointmentLengthErrorLbl = UILabel(frame: CGRect(x: 10, y: 285, width: self.view.frame.size.width - 20, height: 25))
        appointmentLengthErrorLbl.text = "TESTING - Needs to be removed"
        appointmentLengthErrorLbl.textAlignment = NSTextAlignment.center
        appointmentLengthErrorLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentLengthErrorLbl)
        
        //Decide how to determine the Employee for request
        appointmentRequest = UITextField(frame: CGRect(x: 160, y: 340, width: 180, height: 45))
        appointmentRequest.placeholder = "Enter employee's name"
        appointmentRequest.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentRequest)
        
        //Label for Requesting an Employee
        appointmentRequestLbl = UILabel(frame: CGRect(x: 10, y: 340, width: 140, height: 45))
        appointmentRequestLbl.text = "Employee Requested:"
        appointmentRequestLbl.textAlignment = NSTextAlignment.left
        appointmentRequestLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentRequestLbl)
        
        //Button to create user with username and password and email
        let addAppointmentBtn = UIButton(type: UIButtonType.system)
        addAppointmentBtn.frame = CGRect(x: 10, y: 400, width: self.view.frame.size.width - 20, height: 45)
        addAppointmentBtn.setTitle("Create Appointment", for: UIControlState.normal)
        addAppointmentBtn.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(addAppointmentBtn)
        addAppointmentBtn.addTarget(self, action: #selector(addAppointmentBtnTouched(btn:)), for: UIControlEvents.touchUpInside)
        
    }
    
    func addAppointmentBtnTouched(btn:UIButton) -> Void
    {
        // Need to verify the appointment time, length and employee are valid
        // If not valid, display error stating what is not allowed
        // If true, save information to serve and then push to the View Controller
        let obj = PFObject(className: "appointment")
        obj["aptGuestName"] = guestName.text
        obj["aptDate"] = appointmentDatePicker.date
        obj["aptTime"] = appointmentTimePicker.date
        obj["aptLength"] = appointmentLength.text
        if appointmentRequest.text == "" {
            // Search for the highest prioirty agent with an opening for that date and time
        }
        else
        {
            //if requested employee already has a non requested appointment, then that guest will be pushed to the next priority employee for that day
            obj["aptRequest"] = appointmentRequest.text
        }
        
        obj.saveInBackground { (success, error) in
                print("added")
        }
    
        // Push to the View Appointment view
        let vc = ViewAppointmentVC()
        vc.passedGuest = guestName.text!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy"
        let newDate = dateFormatter.string(from: appointmentDatePicker.date)
        vc.passedDate = newDate
        dateFormatter.dateFormat = "hh:mm"
        let newTime = dateFormatter.string(from: appointmentTimePicker.date)
        vc.passedTime = newTime

        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
