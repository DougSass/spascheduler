//
//  EditEmployeeVC.swift
//  SpaScheduler
//
//  Created by Douglas Sass on 4/15/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit
import Parse

class EditEmployeeVC: UIViewController {
    
    var passedEmployee = ""
    
    var editEmployeeLbl = UILabel()
    
    var employeeName = UITextField()
    var employeeNameLbl = UILabel()
    var employeePriority = UITextField()
    var employeePriorityLbl = UILabel()
    
    // Monday variables
    var mDayOfWeekLbl = UILabel()
    var mStartTime = UITextField()
    var mStartTimeLbl = UILabel()
    var mEndTime = UITextField()
    var mEndTimeLbl = UILabel()
    var mBreakStart = UITextField()
    var mBreakStartLbl = UILabel()
    var mBreakEnd = UITextField()
    var mBreakEndLbl = UILabel()
    
    // Tuesday variables
    var tDayOfWeekLbl = UILabel()
    var tStartTime = UITextField()
    var tStartTimeLbl = UILabel()
    var tEndTime = UITextField()
    var tEndTimeLbl = UILabel()
    var tBreakStart = UITextField()
    var tBreakStartLbl = UILabel()
    var tBreakEnd = UITextField()
    var tBreakEndLbl = UILabel()
    
    // Wednesday variables
    var wDayOfWeekLbl = UILabel()
    var wStartTime = UITextField()
    var wStartTimeLbl = UILabel()
    var wEndTime = UITextField()
    var wEndTimeLbl = UILabel()
    var wBreakStart = UITextField()
    var wBreakStartLbl = UILabel()
    var wBreakEnd = UITextField()
    var wBreakEndLbl = UILabel()
    
    // Thursday variables
    var thDayOfWeekLbl = UILabel()
    var thStartTime = UITextField()
    var thStartTimeLbl = UILabel()
    var thEndTime = UITextField()
    var thEndTimeLbl = UILabel()
    var thBreakStart = UITextField()
    var thBreakStartLbl = UILabel()
    var thBreakEnd = UITextField()
    var thBreakEndLbl = UILabel()
    
    // Friday variables
    var fDayOfWeekLbl = UILabel()
    var fStartTime = UITextField()
    var fStartTimeLbl = UILabel()
    var fEndTime = UITextField()
    var fEndTimeLbl = UILabel()
    var fBreakStart = UITextField()
    var fBreakStartLbl = UILabel()
    var fBreakEnd = UITextField()
    var fBreakEndLbl = UILabel()
    
    // Saturday variables
    var sDayOfWeekLbl = UILabel()
    var sStartTime = UITextField()
    var sStartTimeLbl = UILabel()
    var sEndTime = UITextField()
    var sEndTimeLbl = UILabel()
    var sBreakStart = UITextField()
    var sBreakStartLbl = UILabel()
    var sBreakEnd = UITextField()
    var sBreakEndLbl = UILabel()
    
    var editEmployeeBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let scrollview = UIScrollView(frame: self.view.bounds)
        scrollview.backgroundColor = UIColor.lightGray
        self.view.addSubview(scrollview)
        scrollview.contentSize = CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height * 4)
        
        let nameView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.frame.size.height / 2))
        scrollview.addSubview(nameView)
        
        //Label for Add Employee Title
        editEmployeeLbl = UILabel(frame: CGRect(x: 10, y: 30, width: self.view.frame.size.width - 20, height: 45))
        editEmployeeLbl.text = "Edit Employee"
        editEmployeeLbl.textAlignment = NSTextAlignment.center
        editEmployeeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        nameView.addSubview(editEmployeeLbl)
        
        //Text field for the Username for sign up
        employeeName = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        employeeName.placeholder = "Employee Name"
        employeeName.autoresizingMask = UIViewAutoresizing.flexibleWidth
        nameView.addSubview(employeeName)
        
        //Label for username
        employeeNameLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        employeeNameLbl.text = "Employee Name:"
        employeeNameLbl.textAlignment = NSTextAlignment.center
        employeeNameLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        nameView.addSubview(employeeNameLbl)
        
        //Text field for the Username for sign up
        employeePriority = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        employeePriority.placeholder = "Enter a number from 1 to 30"
        employeePriority.autoresizingMask = UIViewAutoresizing.flexibleWidth
        nameView.addSubview(employeePriority)
        
        //Label for username
        employeePriorityLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        employeePriorityLbl.text = "Employee Priority:"
        employeePriorityLbl.textAlignment = NSTextAlignment.center
        employeePriorityLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        nameView.addSubview(employeePriorityLbl)
        
        let mondayView = UIView(frame: CGRect(x: (self.view.bounds.size.height * 4) / (8), y: 0, width: self.view.bounds.size.width, height: self.view.frame.size.height / 2))
        scrollview.addSubview(mondayView)
        
        //Label for Add User Title
        mDayOfWeekLbl = UILabel(frame: CGRect(x: 10, y: 30, width: self.view.frame.size.width - 20, height: 45))
        mDayOfWeekLbl.text = "Monday"
        mDayOfWeekLbl.textAlignment = NSTextAlignment.center
        mDayOfWeekLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        mondayView.addSubview(mDayOfWeekLbl)
        
        //Text field for entering the start time
        mStartTime = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        mStartTime.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        mStartTime.autoresizingMask = UIViewAutoresizing.flexibleWidth
        mondayView.addSubview(mStartTime)
        
        //Label for entering the start time
        mStartTimeLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        mStartTimeLbl.text = "Start Time:"
        mStartTimeLbl.textAlignment = NSTextAlignment.center
        mStartTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        mondayView.addSubview(mStartTimeLbl)
        
        //Text field for entering the End time
        mEndTime = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        mEndTime.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        mEndTime.autoresizingMask = UIViewAutoresizing.flexibleWidth
        mondayView.addSubview(mEndTime)
        
        //Label for entering the end time
        mEndTimeLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        mEndTimeLbl.text = "End Time:"
        mEndTimeLbl.textAlignment = NSTextAlignment.center
        mEndTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        mondayView.addSubview(mEndTimeLbl)
        
        //Text field for entering the break start time
        mBreakStart = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        mBreakStart.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        mBreakStart.autoresizingMask = UIViewAutoresizing.flexibleWidth
        mondayView.addSubview(mBreakStart)
        
        //Label for entering the break start time
        mBreakStartLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        mBreakStartLbl.text = "Break Start Time:"
        mBreakStartLbl.textAlignment = NSTextAlignment.center
        mBreakStartLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        mondayView.addSubview(mBreakStartLbl)
        
        //Text field for entering the break end time
        mBreakEnd = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        mBreakEnd.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        mBreakEnd.autoresizingMask = UIViewAutoresizing.flexibleWidth
        mondayView.addSubview(mBreakEnd)
        
        //Label for entering the break end time
        mBreakEndLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        mBreakEndLbl.text = "Break End Time:"
        mBreakEndLbl.textAlignment = NSTextAlignment.center
        mBreakEndLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        mondayView.addSubview(mBreakEndLbl)
        
        let tuesdayView = UIView(frame: CGRect(x: (self.view.bounds.size.height * 4) / (8/2), y: 0, width: self.view.bounds.size.width, height: self.view.frame.size.height / 2))
        scrollview.addSubview(tuesdayView)
        
        //Label for Add User Title
        tDayOfWeekLbl = UILabel(frame: CGRect(x: 10, y: 30, width: self.view.frame.size.width - 20, height: 45))
        tDayOfWeekLbl.text = "Tuesday"
        tDayOfWeekLbl.textAlignment = NSTextAlignment.center
        tDayOfWeekLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        tuesdayView.addSubview(tDayOfWeekLbl)
        
        //Text field for entering the start time
        tStartTime = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        tStartTime.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        tStartTime.autoresizingMask = UIViewAutoresizing.flexibleWidth
        tuesdayView.addSubview(tStartTime)
        
        //Label for entering the start time
        tStartTimeLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        tStartTimeLbl.text = "Start Time:"
        tStartTimeLbl.textAlignment = NSTextAlignment.center
        tStartTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        tuesdayView.addSubview(tStartTimeLbl)
        
        //Text field for entering the End time
        tEndTime = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        tEndTime.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        tEndTime.autoresizingMask = UIViewAutoresizing.flexibleWidth
        tuesdayView.addSubview(tEndTime)
        
        //Label for entering the end time
        tEndTimeLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        tEndTimeLbl.text = "End Time:"
        tEndTimeLbl.textAlignment = NSTextAlignment.center
        tEndTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        tuesdayView.addSubview(tEndTimeLbl)
        
        //Text field for entering the break start time
        tBreakStart = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        tBreakStart.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        tBreakStart.autoresizingMask = UIViewAutoresizing.flexibleWidth
        tuesdayView.addSubview(tBreakStart)
        
        //Label for entering the break start time
        tBreakStartLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        tBreakStartLbl.text = "Break Start Time:"
        tBreakStartLbl.textAlignment = NSTextAlignment.center
        tBreakStartLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        tuesdayView.addSubview(tBreakStartLbl)
        
        //Text field for entering the break end time
        tBreakEnd = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        tBreakEnd.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        tBreakEnd.autoresizingMask = UIViewAutoresizing.flexibleWidth
        tuesdayView.addSubview(tBreakEnd)
        
        //Label for entering the break end time
        tBreakEndLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        tBreakEndLbl.text = "Break End Time:"
        tBreakEndLbl.textAlignment = NSTextAlignment.center
        tBreakEndLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        tuesdayView.addSubview(tBreakEndLbl)
        
        let wednesdayView = UIView(frame: CGRect(x: (self.view.bounds.size.height * 4) / (8/3), y: 0, width: self.view.bounds.size.width, height: self.view.frame.size.height / 2))
        scrollview.addSubview(wednesdayView)
        
        //Label for Add User Title
        wDayOfWeekLbl = UILabel(frame: CGRect(x: 10, y: 30, width: self.view.frame.size.width - 20, height: 45))
        wDayOfWeekLbl.text = "Wednesday"
        wDayOfWeekLbl.textAlignment = NSTextAlignment.center
        wDayOfWeekLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        wednesdayView.addSubview(wDayOfWeekLbl)
        
        //Text field for entering the start time
        wStartTime = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        wStartTime.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        wStartTime.autoresizingMask = UIViewAutoresizing.flexibleWidth
        wednesdayView.addSubview(wStartTime)
        
        //Label for entering the start time
        wStartTimeLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        wStartTimeLbl.text = "Start Time:"
        wStartTimeLbl.textAlignment = NSTextAlignment.center
        wStartTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        wednesdayView.addSubview(wStartTimeLbl)
        
        //Text field for entering the End time
        wEndTime = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        wEndTime.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        wEndTime.autoresizingMask = UIViewAutoresizing.flexibleWidth
        wednesdayView.addSubview(wEndTime)
        
        //Label for entering the end time
        wEndTimeLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        wEndTimeLbl.text = "End Time:"
        wEndTimeLbl.textAlignment = NSTextAlignment.center
        wEndTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        wednesdayView.addSubview(wEndTimeLbl)
        
        //Text field for entering the break start time
        wBreakStart = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        wBreakStart.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        wBreakStart.autoresizingMask = UIViewAutoresizing.flexibleWidth
        wednesdayView.addSubview(wBreakStart)
        
        //Label for entering the break start time
        wBreakStartLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        wBreakStartLbl.text = "Break Start Time:"
        wBreakStartLbl.textAlignment = NSTextAlignment.center
        wBreakStartLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        wednesdayView.addSubview(wBreakStartLbl)
        
        //Text field for entering the break end time
        wBreakEnd = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        wBreakEnd.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        wBreakEnd.autoresizingMask = UIViewAutoresizing.flexibleWidth
        wednesdayView.addSubview(wBreakEnd)
        
        //Label for entering the break end time
        wBreakEndLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        wBreakEndLbl.text = "Break End Time:"
        wBreakEndLbl.textAlignment = NSTextAlignment.center
        wBreakEndLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        wednesdayView.addSubview(wBreakEndLbl)
        
        let thursdayView = UIView(frame: CGRect(x: (self.view.bounds.size.height * 4) / (8/4), y: 0, width: self.view.bounds.size.width, height: self.view.frame.size.height / 2))
        scrollview.addSubview(thursdayView)
        
        //Label for Add User Title
        thDayOfWeekLbl = UILabel(frame: CGRect(x: 10, y: 30, width: self.view.frame.size.width - 20, height: 45))
        thDayOfWeekLbl.text = "Thursday"
        thDayOfWeekLbl.textAlignment = NSTextAlignment.center
        thDayOfWeekLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        thursdayView.addSubview(thDayOfWeekLbl)
        
        //Text field for entering the start time
        thStartTime = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        thStartTime.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        thStartTime.autoresizingMask = UIViewAutoresizing.flexibleWidth
        thursdayView.addSubview(thStartTime)
        
        //Label for entering the start time
        thStartTimeLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        thStartTimeLbl.text = "Start Time:"
        thStartTimeLbl.textAlignment = NSTextAlignment.center
        thStartTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        thursdayView.addSubview(thStartTimeLbl)
        
        //Text field for entering the End time
        thEndTime = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        thEndTime.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        thEndTime.autoresizingMask = UIViewAutoresizing.flexibleWidth
        thursdayView.addSubview(thEndTime)
        
        //Label for entering the end time
        thEndTimeLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        thEndTimeLbl.text = "End Time:"
        thEndTimeLbl.textAlignment = NSTextAlignment.center
        thEndTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        thursdayView.addSubview(thEndTimeLbl)
        
        //Text field for entering the break start time
        thBreakStart = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        thBreakStart.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        thBreakStart.autoresizingMask = UIViewAutoresizing.flexibleWidth
        thursdayView.addSubview(thBreakStart)
        
        //Label for entering the break start time
        thBreakStartLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        thBreakStartLbl.text = "Break Start Time:"
        thBreakStartLbl.textAlignment = NSTextAlignment.center
        thBreakStartLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        thursdayView.addSubview(thBreakStartLbl)
        
        //Text field for entering the break end time
        thBreakEnd = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        thBreakEnd.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        thBreakEnd.autoresizingMask = UIViewAutoresizing.flexibleWidth
        thursdayView.addSubview(thBreakEnd)
        
        //Label for entering the break end time
        thBreakEndLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        thBreakEndLbl.text = "Break End Time:"
        thBreakEndLbl.textAlignment = NSTextAlignment.center
        thBreakEndLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        thursdayView.addSubview(thBreakEndLbl)
        
        let fridayView = UIView(frame: CGRect(x: (self.view.bounds.size.height * 4) / (8/5), y: 0, width: self.view.bounds.size.width, height: self.view.frame.size.height / 2))
        scrollview.addSubview(fridayView)
        
        //Label for Add User Title
        fDayOfWeekLbl = UILabel(frame: CGRect(x: 10, y: 30, width: self.view.frame.size.width - 20, height: 45))
        fDayOfWeekLbl.text = "Friday"
        fDayOfWeekLbl.textAlignment = NSTextAlignment.center
        fDayOfWeekLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        fridayView.addSubview(fDayOfWeekLbl)
        
        //Text field for entering the start time
        fStartTime = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        fStartTime.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        fStartTime.autoresizingMask = UIViewAutoresizing.flexibleWidth
        fridayView.addSubview(fStartTime)
        
        //Label for entering the start time
        fStartTimeLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        fStartTimeLbl.text = "Start Time:"
        fStartTimeLbl.textAlignment = NSTextAlignment.center
        fStartTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        fridayView.addSubview(fStartTimeLbl)
        
        //Text field for entering the End time
        fEndTime = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        fEndTime.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        fEndTime.autoresizingMask = UIViewAutoresizing.flexibleWidth
        fridayView.addSubview(fEndTime)
        
        //Label for entering the end time
        fEndTimeLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        fEndTimeLbl.text = "End Time:"
        fEndTimeLbl.textAlignment = NSTextAlignment.center
        fEndTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        fridayView.addSubview(fEndTimeLbl)
        
        //Text field for entering the break start time
        fBreakStart = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        fBreakStart.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        fBreakStart.autoresizingMask = UIViewAutoresizing.flexibleWidth
        fridayView.addSubview(fBreakStart)
        
        //Label for entering the break start time
        fBreakStartLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        fBreakStartLbl.text = "Break Start Time:"
        fBreakStartLbl.textAlignment = NSTextAlignment.center
        fBreakStartLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        fridayView.addSubview(fBreakStartLbl)
        
        //Text field for entering the break end time
        fBreakEnd = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        fBreakEnd.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        fBreakEnd.autoresizingMask = UIViewAutoresizing.flexibleWidth
        fridayView.addSubview(fBreakEnd)
        
        //Label for entering the break end time
        fBreakEndLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        fBreakEndLbl.text = "Break End Time:"
        fBreakEndLbl.textAlignment = NSTextAlignment.center
        fBreakEndLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        fridayView.addSubview(fBreakEndLbl)
        
        let saturdayView = UIView(frame: CGRect(x: (self.view.bounds.size.height * 4) / (8/6), y: 0, width: self.view.bounds.size.width, height: self.view.frame.size.height / 2))
        scrollview.addSubview(saturdayView)
        
        //Label for Add User Title
        sDayOfWeekLbl = UILabel(frame: CGRect(x: 10, y: 30, width: self.view.frame.size.width - 20, height: 45))
        sDayOfWeekLbl.text = "Saturday"
        sDayOfWeekLbl.textAlignment = NSTextAlignment.center
        sDayOfWeekLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        saturdayView.addSubview(sDayOfWeekLbl)
        
        //Text field for entering the start time
        sStartTime = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        sStartTime.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        sStartTime.autoresizingMask = UIViewAutoresizing.flexibleWidth
        saturdayView.addSubview(sStartTime)
        
        //Label for entering the start time
        sStartTimeLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        sStartTimeLbl.text = "Start Time:"
        sStartTimeLbl.textAlignment = NSTextAlignment.center
        sStartTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        saturdayView.addSubview(sStartTimeLbl)
        
        //Text field for entering the End time
        sEndTime = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        sEndTime.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        sEndTime.autoresizingMask = UIViewAutoresizing.flexibleWidth
        saturdayView.addSubview(sEndTime)
        
        //Label for entering the end time
        sEndTimeLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        sEndTimeLbl.text = "End Time:"
        sEndTimeLbl.textAlignment = NSTextAlignment.center
        sEndTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        saturdayView.addSubview(sEndTimeLbl)
        
        //Text field for entering the break start time
        sBreakStart = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        sBreakStart.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        sBreakStart.autoresizingMask = UIViewAutoresizing.flexibleWidth
        saturdayView.addSubview(sBreakStart)
        
        //Label for entering the break start time
        sBreakStartLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        sBreakStartLbl.text = "Break Start Time:"
        sBreakStartLbl.textAlignment = NSTextAlignment.center
        sBreakStartLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        saturdayView.addSubview(sBreakStartLbl)
        
        //Text field for entering the break end time
        sBreakEnd = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        sBreakEnd.placeholder = "Enter time in the format 9:00 AM or 9:30 AM"
        sBreakEnd.autoresizingMask = UIViewAutoresizing.flexibleWidth
        saturdayView.addSubview(sBreakEnd)
        
        //Label for entering the break end time
        sBreakEndLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        sBreakEndLbl.text = "Break End Time:"
        sBreakEndLbl.textAlignment = NSTextAlignment.center
        sBreakEndLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        saturdayView.addSubview(sBreakEndLbl)
        
        let editEmployeeView = UIView(frame: CGRect(x: (self.view.bounds.size.height * 4) / (8/7), y: 0, width: self.view.bounds.size.width, height: self.view.frame.size.height / 2))
        scrollview.addSubview(editEmployeeView)
        
        //Button to create user with username and password and email
        let editEmployeeBtn = UIButton(type: UIButtonType.system)
        editEmployeeBtn.frame = CGRect(x: 10, y: 300, width: self.view.frame.size.width - 20, height: 45)
        editEmployeeBtn.setTitle("Add Employee", for: UIControlState.normal)
        editEmployeeBtn.autoresizingMask = UIViewAutoresizing.flexibleWidth
        editEmployeeView.addSubview(editEmployeeBtn)
        editEmployeeBtn.addTarget(self, action: #selector(editEmployeeBtnTouched(btn:)), for: UIControlEvents.touchUpInside)
        
    }
    
    func editEmployeeBtnTouched(btn:UIButton) -> Void
    {
        // Verify the start and end time's are valid
        // Save employee information
        let obj = PFObject(className: "employee")
        obj["employeeName"] = employeeName.text
        obj["employeePriority"] = employeePriority.text
        
        obj["monStart"] =  mStartTime.text
        obj["monEnd"] = mEndTime.text
        obj["monBreakStart"] = mBreakStart.text
        obj["monBreakEnd"] = mBreakEnd.text
        
        obj["tueStart"] = tStartTime.text
        obj["tueEnd"] = tEndTime.text
        obj["tueBreakStart"] = tBreakStart.text
        obj["tueBreakEnd"] = tBreakEnd.text
        
        obj["wedStart"] = wStartTime.text
        obj["wedEnd"] = wEndTime.text
        obj["wedBreakStart"] = wBreakStart.text
        obj["wedBreadEnd"] = wBreakEnd.text
        
        obj["thuStart"] = thStartTime.text
        obj["thuEnd"] = thEndTime.text
        obj["thuBreakStart"] = thBreakStart.text
        obj["thuBreakEnd"] = thBreakEnd.text
        
        obj["friStart"] = fStartTime.text
        obj["friEnd"] = fEndTime.text
        obj["friBreakStart"] = fBreakStart.text
        obj["FriBreakEnd"] = fBreakEnd.text
        
        obj["satStart"] = sStartTime.text
        obj["satEnd"] = sEndTime.text
        obj["satBreakStart"] = sBreakStart.text
        obj["satBreakEnd"] = sBreakEnd.text
        
        obj.saveInBackground { (success, error) in
            print("added")
        }
        
        //push to the View employees view
        let vc = ViewEmployeesTVC()
        
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
