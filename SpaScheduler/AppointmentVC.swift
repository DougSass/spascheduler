//
//  AppointmentVC.swift
//  SpaScheduler
//
//  Created by Douglas Sass on 4/15/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit
import Parse

class AppointmentVC: UIViewController {

    var appointmentsTitleLbl = UILabel()
    var appointmentSeperatorLbl = UILabel()
    var employeeSeperatorLbl = UILabel()

    var addAppointmentBtn = UIButton()
    var editAppointmentBtn = UIButton()
    var viewScheuldeBtn = UIButton()
    var viewEmployeeBtn = UIButton()
    var createUserBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.white
        
        // Label for Appointments as Title of View
        appointmentsTitleLbl = UILabel(frame: CGRect(x: 10, y: 50, width: self.view.frame.size.width - 20, height: 45))
        appointmentsTitleLbl.text = "Appointments"
        appointmentsTitleLbl.textAlignment = NSTextAlignment.center
        appointmentsTitleLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentsTitleLbl)
        
        // Button to go to View Schedule
        let viewScheuldeBtn = UIButton(type: UIButtonType.system)
        viewScheuldeBtn.frame = CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45)
        viewScheuldeBtn.setTitle("View Schedule", for: UIControlState.normal)
        viewScheuldeBtn.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(viewScheuldeBtn)
        viewScheuldeBtn.addTarget(self, action: #selector(viewScheuldeBtnTouched(btn:)), for: UIControlEvents.touchUpInside)
        
        // Label for separating Appointment buttons
        appointmentSeperatorLbl = UILabel(frame: CGRect(x: 10, y: 120, width: self.view.frame.size.width - 20, height: 45))
        appointmentSeperatorLbl.text = "Appointment Options"
        appointmentSeperatorLbl.textAlignment = NSTextAlignment.center
        appointmentSeperatorLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentSeperatorLbl)
        
        // Button to add an Appointment
        let addAppointmentBtn = UIButton(type: UIButtonType.system)
        addAppointmentBtn.frame = CGRect(x: 10, y: 150, width: self.view.frame.size.width - 20, height: 45)
        addAppointmentBtn.setTitle("Add Appointment", for: UIControlState.normal)
        addAppointmentBtn.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(addAppointmentBtn)
        addAppointmentBtn.addTarget(self, action: #selector(addAppointmentBtnTouched(btn:)), for: UIControlEvents.touchUpInside)
        
        // Button to Edit an Appointment
        let editAppointmentBtn = UIButton(type: UIButtonType.system)
        editAppointmentBtn.frame = CGRect(x: 10, y: 200, width: self.view.frame.size.width - 20, height: 45)
        editAppointmentBtn.setTitle("Search Appointment", for: UIControlState.normal)
        editAppointmentBtn.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(editAppointmentBtn)
        editAppointmentBtn.addTarget(self, action: #selector(editAppointmentBtnTouched(btn:)), for: UIControlEvents.touchUpInside)
        
        // Label for separating employee buttons
        employeeSeperatorLbl = UILabel(frame: CGRect(x: 10, y: 250, width: self.view.frame.size.width - 20, height: 45))
        employeeSeperatorLbl.text = "Employee Options"
        employeeSeperatorLbl.textAlignment = NSTextAlignment.center
        employeeSeperatorLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(employeeSeperatorLbl)
        
        // Button to View Employees
        let viewEmployeeBtn = UIButton(type: UIButtonType.system)
        viewEmployeeBtn.frame = CGRect(x: 10, y: 300, width: self.view.frame.size.width - 20, height: 45)
        viewEmployeeBtn.setTitle("View Employee", for: UIControlState.normal)
        viewEmployeeBtn.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(viewEmployeeBtn)
        viewEmployeeBtn.addTarget(self, action: #selector(viewEmployeeBtnTouched(btn:)), for: UIControlEvents.touchUpInside)
        
        // Button to View Employees
        let createUserBtn = UIButton(type: UIButtonType.system)
        createUserBtn.frame = CGRect(x: 10, y: 350, width: self.view.frame.size.width - 20, height: 45)
        createUserBtn.setTitle("Create User", for: UIControlState.normal)
        createUserBtn.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(createUserBtn)
        createUserBtn.addTarget(self, action: #selector(createUserBtnTouched(btn:)), for: UIControlEvents.touchUpInside)
        
    }
    
    func addAppointmentBtnTouched(btn:UIButton) -> Void
    {
        //push to the add appointment view
        let vc = AddAppointmentVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func editAppointmentBtnTouched(btn:UIButton) -> Void
    {
        //push to the edit Appointment view
        let vc = SearchAppointmentVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewScheuldeBtnTouched(btn:UIButton) -> Void
    {
        //push to the View Schedule view
        let vc = SearchScheduleVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewEmployeeBtnTouched(btn:UIButton) -> Void
    {
        //push to the edit Employee view
        let vc = ViewEmployeesTVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func createUserBtnTouched(btn:UIButton) -> Void
    {
        //push to the appointment view
        let vc = NewUserVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
