//
//  SearchAppointmentVC.swift
//  SpaScheduler
//
//  Created by Douglas Sass on 4/15/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit
import Parse

class SearchAppointmentVC: UIViewController {

    var searchAppointmentLbl = UILabel()
    
    var guestName = UITextField()
    var guestNameLbl = UILabel()
    
    var appointmentDatePicker = UIDatePicker()
    var appointmentDateLbl = UILabel()
    
    var appointmentTimePicker = UIDatePicker()
    var appointmentTimeLbl = UILabel()
    
    var searchAppointmentBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.white
        
        //Label for Search Appointment Title
        searchAppointmentLbl = UILabel(frame: CGRect(x: 10, y: 50, width: self.view.frame.size.width - 20, height: 45))
        searchAppointmentLbl.text = "Search Appointment"
        searchAppointmentLbl.textAlignment = NSTextAlignment.center
        searchAppointmentLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(searchAppointmentLbl)
        
        //Text field for the guest name for the search
        guestName = UITextField(frame: CGRect(x: 10, y: 120, width: self.view.frame.size.width - 20, height: 45))
        guestName.placeholder = "GuestName"
        guestName.textAlignment = NSTextAlignment.center
        guestName.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(guestName)
        
        //Label for Guest Name
        guestNameLbl = UILabel(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        guestNameLbl.text = "Enter Guest's Name:"
        guestNameLbl.textAlignment = NSTextAlignment.center
        guestNameLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(guestNameLbl)
        
        // Pick date for the search
        appointmentDatePicker = UIDatePicker(frame: CGRect(x: 10, y: 180, width: self.view.frame.size.width - 20, height: 45))
        self.view.addSubview(appointmentDatePicker)
        
        // Label for the Date
        appointmentDateLbl = UILabel(frame: CGRect(x: 10, y: 140, width: self.view.frame.size.width - 20, height: 45))
        appointmentDateLbl.text = "Date of Appointment:"
        appointmentDateLbl.textAlignment = NSTextAlignment.center
        appointmentDateLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentDateLbl)
        
        //Text field for the Time of the appointment
        appointmentTimePicker = UIDatePicker(frame: CGRect(x: 10, y: 260, width: self.view.frame.size.width, height: 45))
        appointmentTimePicker.datePickerMode = UIDatePickerMode.time
        appointmentTimePicker.minuteInterval = 30
        appointmentTimePicker.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentTimePicker)
        
        self.view.addSubview(appointmentTimeLbl)
        // Label for the time of the appointment
        appointmentTimeLbl = UILabel(frame: CGRect(x: 10, y: 220, width: self.view.frame.size.width - 20, height: 45))
        appointmentTimeLbl.text = "Time of Appointment:"
        appointmentTimeLbl.textAlignment = NSTextAlignment.center
        appointmentTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentTimeLbl)
        
        // Button for searching for the appointment
        let searchAppointmentBtn = UIButton(type: UIButtonType.system)
        searchAppointmentBtn.frame = CGRect(x: 10, y: 350, width: self.view.frame.size.width - 20, height: 45)
        searchAppointmentBtn.setTitle("Search", for: UIControlState.normal)
        searchAppointmentBtn.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(searchAppointmentBtn)
        searchAppointmentBtn.addTarget(self, action: #selector(searchAppointmentBtnTouched(btn:)), for: UIControlEvents.touchUpInside)
        
    }
    
    func searchAppointmentBtnTouched(btn:UIButton) -> Void
    {
        // Need to verify data entered is valid,
        // Then verify their is an appointment for that date
        //When both are valid, push to view the appointment
        
        // Push to the View Appointment view
        let vc = ViewAppointmentVC()
        vc.passedGuest = guestName.text!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy"
        let newDate = dateFormatter.string(from: appointmentDatePicker.date)
        vc.passedDate = newDate
        
        // vc.passedTime = appointmentTime.text!
        self.navigationController?.pushViewController(vc, animated: true)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
