//
//  NewUserVC.swift
//  SpaScheduler
//
//  Created by Douglas Sass on 4/15/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit
import Parse

class NewUserVC: UIViewController {
    
    var addUserLbl = UILabel()
    
    var usernameCreate = UITextField()
    var usernameCreateLbl = UILabel()
    
    var passwordCreate = UITextField()
    var passwordCreateLbl = UILabel()
    
    var emailCreate = UITextField()
    var emailCreateLbl = UILabel()
    
    var addUserBtn = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.white
        
        //Label for Add User Title
        usernameCreateLbl = UILabel(frame: CGRect(x: 10, y: 30, width: self.view.frame.size.width - 20, height: 45))
        usernameCreateLbl.text = "Set-Up New User"
        usernameCreateLbl.textAlignment = NSTextAlignment.center
        usernameCreateLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(usernameCreateLbl)
        
        //Text field for the Username for sign up
        usernameCreate = UITextField(frame: CGRect(x: 160, y: 125, width: 180, height: 45))
        usernameCreate.placeholder = "Username"
        usernameCreate.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(usernameCreate)
        
        //Label for username
        usernameCreateLbl = UILabel(frame: CGRect(x: 10, y: 125, width: 140, height: 45))
        usernameCreateLbl.text = "Create Username"
        usernameCreateLbl.textAlignment = NSTextAlignment.center
        usernameCreateLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(usernameCreateLbl)
        
        //Text field for the Password for sign up
        passwordCreate = UITextField(frame: CGRect(x: 160, y: 210, width: 180, height: 45))
        passwordCreate.placeholder = "Password"
        passwordCreate.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(passwordCreate)
        
        //Label for password
        passwordCreateLbl = UILabel(frame: CGRect(x: 10, y: 210, width: 140, height: 45))
        passwordCreateLbl.text = "Create Password"
        passwordCreateLbl.textAlignment = NSTextAlignment.center
        passwordCreateLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(passwordCreateLbl)
        
        //Text field for the email for sign up
        emailCreate = UITextField(frame: CGRect(x: 160, y: 295, width: 180, height: 45))
        emailCreate.placeholder = "email@email.com"
        emailCreate.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(emailCreate)
        
        //Label for Email
        emailCreateLbl = UILabel(frame: CGRect(x: 10, y: 295, width: 140, height: 45))
        emailCreateLbl.text = "Enter Email"
        emailCreateLbl.textAlignment = NSTextAlignment.center
        emailCreateLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(emailCreateLbl)
        
        //Button to create user with username and password and email
        let addUserBtn = UIButton(type: UIButtonType.system)
        addUserBtn.frame = CGRect(x: 10, y: 325, width: self.view.frame.size.width - 20, height: 45)
        addUserBtn.setTitle("Create User", for: UIControlState.normal)
        addUserBtn.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(addUserBtn)
        addUserBtn.addTarget(self, action: #selector(addUserBtnTouched(btn:)), for: UIControlEvents.touchUpInside)
        
    }
    
    func addUserBtnTouched(btn:UIButton) -> Void
    {
        let user = PFUser()
        user.username = usernameCreate.text
        user.password = passwordCreate.text
        user.email = emailCreate.text
        user.signUpInBackground { (success, error) in
            print("I signed up")
        }
        //push to the appointment view
        let vc = AppointmentVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
