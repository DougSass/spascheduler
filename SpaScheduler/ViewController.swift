//
//  ViewController.swift
//  SpaScheduler
//
//  Created by Douglas Sass on 4/4/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController {

    var loginTitleLbl = UILabel()
    
    var usernameLogin = UITextField()
    var usernameLbl = UILabel()
    
    var passwordLogin = UITextField()
    var passwordLbl = UILabel()
    
    var loginBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let currentUser = PFUser.current()
        if currentUser != nil {
            // Do stuff with the user
            // Push to table view
        } else {
            // Show the signup or login screen
        }
        

        
        // Label as Title of Sign in view
        loginTitleLbl = UILabel(frame: CGRect(x: 10, y: 75, width: self.view.frame.size.width - 20, height: 45))
        loginTitleLbl.text = "Sign In"
        loginTitleLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        loginTitleLbl.textAlignment = NSTextAlignment.center
        self.view.addSubview(loginTitleLbl)
        
        //Text field for the Username for Login
        usernameLogin = UITextField(frame: CGRect(x: 160, y: 125, width: 180, height: 45))
        usernameLogin.placeholder = "Username"
        usernameLogin.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(usernameLogin)
        
        //Label for username
        usernameLbl = UILabel(frame: CGRect(x: 10, y: 125, width: 140, height: 45))
        usernameLbl.text = "Enter Username:"
        usernameLbl.textAlignment = NSTextAlignment.right
        self.view.addSubview(usernameLbl)
        
        //Text field for the Password for Login
        passwordLogin = UITextField(frame: CGRect(x: 160, y: 210, width: 180, height: 45))
        passwordLogin.placeholder = "Password"
        passwordLogin.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(passwordLogin)
        
        //Label for password
        passwordLbl = UILabel(frame: CGRect(x: 10, y: 210, width: 140, height: 45))
        passwordLbl.text = "Enter Password:"
        passwordLbl.textAlignment = NSTextAlignment.right
        self.view.addSubview(passwordLbl)
        
        //Button to login with username and password
        let loginBtn = UIButton(type: UIButtonType.system)
        loginBtn.frame = CGRect(x: 10, y: 250, width: self.view.frame.size.width - 20, height: 45)
        loginBtn.setTitle("Login", for: UIControlState.normal)
        loginBtn.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(loginBtn)
        loginBtn.addTarget(self, action: #selector(loginBtnTouched(btn1:)), for: UIControlEvents.touchUpInside)

    }
    
    func loginBtnTouched(btn1:UIButton) -> Void
    {
//        PFUser.logInWithUsername(inBackground: usernameLogin.text!, password: passwordLogin.text!) { (User, error) in
//            print(User!)
            let vc = AppointmentVC()
            self.navigationController?.pushViewController(vc, animated: true)
//        }
        
        //Push to the tableview
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

