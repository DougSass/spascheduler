//
//  ViewAppointmentVC.swift
//  SpaScheduler
//
//  Created by Douglas Sass on 4/15/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit
import Parse

class ViewAppointmentVC: UIViewController {

    var editAppointmentLbl = UILabel()
    
    var guestNameEnteredLbl = UILabel()
    var guestNameLbl = UILabel()
    
    var appointmentDatePickedLbl = UILabel()
    var appointmentDateLbl = UILabel()

    var appointmentTimeLbl = UILabel()
    var appointmentTimeSelectedLbl = UILabel()

    var appointmentLengthLbl = UILabel()
    var appointmentLengthSelectedLbl = UILabel()
    
    var appointmentRequestLbl = UILabel()
    var appointmentRequestSelectedLbl = UILabel()
    
    var editAppointmentBtn = UIButton()
    
    var closeAppointmentBtn = UIButton()
    
    var passedGuest = ""
    var passedDate = ""
    var passedTime = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.white
        
        //Label for Edit the appointment
        editAppointmentLbl = UILabel(frame: CGRect(x: 10, y: 30, width: self.view.frame.size.width - 20, height: 45))
        editAppointmentLbl.text = "Edit the Appointment"
        editAppointmentLbl.textAlignment = NSTextAlignment.center
        editAppointmentLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(editAppointmentLbl)
        
        //Label with guest name
        guestNameEnteredLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        guestNameEnteredLbl.text = passedGuest
        guestNameEnteredLbl.textAlignment = NSTextAlignment.center
        guestNameEnteredLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(guestNameEnteredLbl)
        
        // Label for the guest name
        guestNameLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        guestNameLbl.text = "Guest's Name:"
        guestNameLbl.textAlignment = NSTextAlignment.center
        guestNameLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(guestNameLbl)
        
        //Label for date of appointment
        appointmentDateLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        appointmentDateLbl.text = "Date of Appointment:"
        appointmentDateLbl.textAlignment = NSTextAlignment.center
        appointmentDateLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentDateLbl)
        
        // Label with the appointment date
        appointmentDatePickedLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        appointmentDatePickedLbl.text = passedDate
        appointmentDatePickedLbl.textAlignment = NSTextAlignment.center
        appointmentDatePickedLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentDatePickedLbl)
        
        //Label for appointment Time
        appointmentTimeLbl = UILabel(frame: CGRect(x: 10, y: 130, width: self.view.frame.size.width - 20, height: 45))
        appointmentTimeLbl.text = "Time of Appointment:"
        appointmentTimeLbl.textAlignment = NSTextAlignment.center
        appointmentTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentTimeLbl)
        
        // Label with the appointment time
        appointmentTimeSelectedLbl = UILabel(frame: CGRect(x: 10, y: 130, width: self.view.frame.size.width - 20, height: 45))
        appointmentTimeSelectedLbl.text = passedTime
        appointmentTimeSelectedLbl.textAlignment = NSTextAlignment.center
        appointmentTimeSelectedLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentTimeSelectedLbl)
        
        //Label for appointment Length
        appointmentLengthLbl = UILabel(frame: CGRect(x: 10, y: 205, width: self.view.frame.size.width - 20, height: 45))
        appointmentLengthLbl.text = "Length of Appointment:"
        appointmentLengthLbl.textAlignment = NSTextAlignment.center
        appointmentLengthLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentLengthLbl)
        
        // Label with the appointment length
        appointmentLengthSelectedLbl = UILabel(frame: CGRect(x: 10, y: 205, width: self.view.frame.size.width - 20, height: 45))
        appointmentLengthSelectedLbl.text = ""
        appointmentLengthSelectedLbl.textAlignment = NSTextAlignment.center
        appointmentLengthSelectedLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentLengthSelectedLbl)
        
        //Label for the Employee request
        appointmentRequestLbl = UILabel(frame: CGRect(x: 10, y: 205, width: self.view.frame.size.width - 20, height: 45))
        appointmentRequestLbl.text = "Employee Request:"
        appointmentRequestLbl.textAlignment = NSTextAlignment.center
        appointmentRequestLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentRequestLbl)
        
        // Label with the employee requested
        appointmentRequestSelectedLbl = UILabel(frame: CGRect(x: 10, y: 205, width: self.view.frame.size.width - 20, height: 45))
        appointmentRequestSelectedLbl.text = ""
        appointmentRequestSelectedLbl.textAlignment = NSTextAlignment.center
        appointmentRequestSelectedLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentRequestSelectedLbl)
        
        //Button to edit the appointment
        let editAppointmentBtn = UIButton(type: UIButtonType.system)
        editAppointmentBtn.frame = CGRect(x: 10, y: 300, width: self.view.frame.size.width - 20, height: 45)
        editAppointmentBtn.setTitle("Edit the Appointment", for: UIControlState.normal)
        editAppointmentBtn.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(editAppointmentBtn)
        editAppointmentBtn.addTarget(self, action: #selector(editAppointmentBtnTouched(btn:)), for: UIControlEvents.touchUpInside)
        
        //Button to close the appointment
        let closeAppointmentBtn = UIButton(type: UIButtonType.system)
        closeAppointmentBtn.frame = CGRect(x: 10, y: 300, width: self.view.frame.size.width - 20, height: 45)
        closeAppointmentBtn.setTitle("Edit the Appointment", for: UIControlState.normal)
        closeAppointmentBtn.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(closeAppointmentBtn)
        closeAppointmentBtn.addTarget(self, action: #selector(closeAppointmentBtnTouched(btn:)), for: UIControlEvents.touchUpInside)
        
    }
    
    func editAppointmentBtnTouched(btn:UIButton) -> Void
    {
        let vc = EditAppointmentVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func closeAppointmentBtnTouched(btn:UIButton) -> Void
    {
        let vc = AppointmentVC()
        self.navigationController!.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
