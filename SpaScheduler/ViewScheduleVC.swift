//
//  ViewScheduleVC.swift
//  SpaScheduler
//
//  Created by Douglas Sass on 4/15/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit
import Parse

class ViewScheduleVC: UIViewController {
    
    var passedEmployee = ""
    var passedDate = ""
    
    var dateAndEmployee = String() // Date and the Employee combined in single String
    
    var time900Lbl = UILabel()
    var guest900Lbl = UILabel()
    var employee900Lbl = UILabel()
    var guest900 = String()
    var employee900 = String()
    
    var time930Lbl = UILabel()
    var guest930Lbl = UILabel()
    var employee930Lbl = UILabel()
    var guest930 = String()
    var employee930 = String()
    
    var time1000Lbl = UILabel()
    var guest1000Lbl = UILabel()
    var employee1000Lbl = UILabel()
    var guest1000 = String()
    var employee1000 = String()
    
    var time1030Lbl = UILabel()
    var guest1030Lbl = UILabel()
    var employee1030Lbl = UILabel()
    var guest1030 = String()
    var employee1030 = String()
    
    var time1100Lbl = UILabel()
    var guest1100Lbl = UILabel()
    var employee1100Lbl = UILabel()
    var guest1100 = String()
    var employee1100 = String()
    
    var time1130Lbl = UILabel()
    var guest1130Lbl = UILabel()
    var employee1130Lbl = UILabel()
    var guest1130 = String()
    var employee1130 = String()
    
    var time1200Lbl = UILabel()
    var guest1200Lbl = UILabel()
    var employee1200Lbl = UILabel()
    var guest1200 = String()
    var employee1200 = String()
    
    var time1230Lbl = UILabel()
    var guest1230Lbl = UILabel()
    var employee1230Lbl = UILabel()
    var guest1230 = String()
    var employee1230 = String()
    
    var time100Lbl = UILabel()
    var guest100Lbl = UILabel()
    var employee100Lbl = UILabel()
    var guest100 = String()
    var employee100 = String()
    
    var time130Lbl = UILabel()
    var guest130Lbl = UILabel()
    var employee130Lbl = UILabel()
    var guest130 = String()
    var employee130 = String()
    
    var time200Lbl = UILabel()
    var guest200Lbl = UILabel()
    var employee200Lbl = UILabel()
    var guest200 = String()
    var employee200 = String()
    
    var time230Lbl = UILabel()
    var guest230Lbl = UILabel()
    var employee230Lbl = UILabel()
    var guest230 = String()
    var employee230 = String()
    
    var time300Lbl = UILabel()
    var guest300Lbl = UILabel()
    var employee300Lbl = UILabel()
    var guest300 = String()
    var employee300 = String()
    
    var time330Lbl = UILabel()
    var guest330Lbl = UILabel()
    var employee330Lbl = UILabel()
    var guest330 = String()
    var employee330 = String()
    
    var time400Lbl = UILabel()
    var guest400Lbl = UILabel()
    var employee400Lbl = UILabel()
    var guest400 = String()
    var employee400 = String()
    
    var time430Lbl = UILabel()
    var guest430Lbl = UILabel()
    var employee430Lbl = UILabel()
    var guest430 = String()
    var employee430 = String()
    
    var time500Lbl = UILabel()
    var guest500Lbl = UILabel()
    var employee500Lbl = UILabel()
    var guest500 = String()
    var employee500 = String()
    
    var time530Lbl = UILabel()
    var guest530Lbl = UILabel()
    var employee530Lbl = UILabel()
    var guest530 = String()
    var employee530 = String()
    
    var time600Lbl = UILabel()
    var guest600Lbl = UILabel()
    var employee600Lbl = UILabel()
    var guest600 = String()
    var employee600 = String()
    
    var time630Lbl = UILabel()
    var guest630Lbl = UILabel()
    var employee630Lbl = UILabel()
    var guest630 = String()
    var employee630 = String()
    
    var time700Lbl = UILabel()
    var guest700Lbl = UILabel()
    var employee700Lbl = UILabel()
    var guest700 = String()
    var employee700 = String()
    
    var time730Lbl = UILabel()
    var guest730Lbl = UILabel()
    var employee730Lbl = UILabel()
    var guest730 = String()
    var employee730 = String()
    
    var time800Lbl = UILabel()
    var guest800Lbl = UILabel()
    var employee800Lbl = UILabel()
    var guest800 = String()
    var employee800 = String()
    
    var time830Lbl = UILabel()
    var guest830Lbl = UILabel()
    var employee830Lbl = UILabel()
    var guest830 = String()
    var employee830 = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Display the date and employee in navigation bar
        navigationItem.title = dateAndEmployee

        let scrollview = UIScrollView(frame: self.view.bounds)
        scrollview.backgroundColor = UIColor.white
        self.view.addSubview(scrollview)
        
        scrollview.contentSize = CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height * 6)
        
        // Time Slot for 9 AM to 9:30 AM
        let view900 = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time900Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time900Lbl.textAlignment = NSTextAlignment.left
        time900Lbl.text = "- 9:00 AM -"
        guest900Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        guest900Lbl.textAlignment = NSTextAlignment.left
        employee900Lbl = UILabel(frame: CGRect(x: 10, y: 130, width: self.view.bounds.size.width, height: 45))
        employee900Lbl.textAlignment = NSTextAlignment.left
        guest900Lbl.text = guest900
        employee900Lbl.text = employee900
        view900.backgroundColor = UIColor.lightGray
        scrollview.addSubview(view900)
        view900.addSubview(time900Lbl)
        view900.addSubview(guest900Lbl)
        view900.addSubview(employee900Lbl)
        
        // Time Slot for 9:30 AM to 10 AM
        let view930 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/1), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time930Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time930Lbl.textAlignment = NSTextAlignment.left
        time930Lbl.text = "- 9:30 AM -"
        guest930Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest930Lbl.textAlignment = NSTextAlignment.left
        employee930Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee930Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest930 == guest900 && employee930 == employee900)
//        {
//            guest930Lbl.text = ""
//            employee930Lbl.text = ""
//            view930.backgroundColor = view900.backgroundColor
//        }
//        else
//        {
            guest930Lbl.text = guest930
            employee930Lbl.text = employee930
            view930.backgroundColor = UIColor.white
//        }
        
        scrollview.addSubview(view930)
        view930.addSubview(time930Lbl)
        view930.addSubview(guest930Lbl)
        view930.addSubview(employee930Lbl)
        
        // Time Slot for 10:00 AM to 10:30 AM
        let view1000 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/2), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time1000Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time1000Lbl.textAlignment = NSTextAlignment.left
        time1000Lbl.text = "- 10:00 AM -"
        guest1000Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest1000Lbl.textAlignment = NSTextAlignment.left
        employee1000Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee1000Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest1000 == guest930 && employee1000 == employee930)
//        {
//            guest1000Lbl.text = ""
//            employee1000Lbl.text = ""
//            view1000.backgroundColor = view930.backgroundColor
//        }
//        else
//        {
            guest1000Lbl.text = guest1000
            employee1000Lbl.text = employee1000
            view1000.backgroundColor = UIColor.lightGray
//        }
        
        scrollview.addSubview(view1000)
        view1000.addSubview(time1000Lbl)
        view1000.addSubview(guest1000Lbl)
        view1000.addSubview(employee1000Lbl)
        
        // Time Slot for 10:30 AM to 11 AM
        let view1030 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/3), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time1030Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time1030Lbl.textAlignment = NSTextAlignment.left
        time1030Lbl.text = "- 10:30 AM -"
        guest1030Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest1030Lbl.textAlignment = NSTextAlignment.left
        employee1030Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee1030Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest1030 == guest1000 && employee1030 == employee1000)
//        {
//            guest1030Lbl.text = ""
//            employee1030Lbl.text = ""
//            view1030.backgroundColor = view1000.backgroundColor
//        }
//        else
//        {
            guest1030Lbl.text = guest1030
            employee1030Lbl.text = employee1030
            view1030.backgroundColor = UIColor.white
//        }
        
        scrollview.addSubview(view1030)
        view1030.addSubview(time1030Lbl)
        view1030.addSubview(guest1030Lbl)
        view1030.addSubview(employee1030Lbl)
        
        // Time Slot for 11:00 AM to 11:30 AM
        let view1100 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/4), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time1100Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time1100Lbl.textAlignment = NSTextAlignment.left
        time1100Lbl.text = "- 11:00 AM -"
        guest1100Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest1100Lbl.textAlignment = NSTextAlignment.left
        employee1100Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee1100Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest1100 == guest1030 && employee1100 == employee1030)
//        {
//            guest1100Lbl.text = ""
//            employee1100Lbl.text = ""
//            view1100.backgroundColor = view1030.backgroundColor
//        }
//        else
//        {
            guest1100Lbl.text = guest1100
            employee1100Lbl.text = employee1100
            view1100.backgroundColor = UIColor.lightGray
//        }
        
        scrollview.addSubview(view1100)
        view1100.addSubview(time1100Lbl)
        view1100.addSubview(guest1100Lbl)
        view1100.addSubview(employee1100Lbl)
        
        // Time Slot for 11:30 AM to 12 PM
        let view1130 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/5), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time1130Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time1130Lbl.textAlignment = NSTextAlignment.left
        time1130Lbl.text = "- 11:30 AM -"
        guest1130Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest1130Lbl.textAlignment = NSTextAlignment.left
        employee1130Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee1130Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest1130 == guest1100 && employee1130 == employee1100)
//        {
//            guest1130Lbl.text = ""
//            employee1130Lbl.text = ""
//            view1130.backgroundColor = view1100.backgroundColor
//        }
//        else
//        {
            guest1130Lbl.text = guest1130
            employee1130Lbl.text = employee1130
            view1130.backgroundColor = UIColor.white
//        }
        
        scrollview.addSubview(view1130)
        view1130.addSubview(time1130Lbl)
        view1130.addSubview(guest1130Lbl)
        view1130.addSubview(employee1130Lbl)
        
        // Time Slot for 12:00 PM to 12:30 PM
        let view1200 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/6), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time1200Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time1200Lbl.textAlignment = NSTextAlignment.left
        time1200Lbl.text = "- 12:00 PM -"
        guest1200Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest1200Lbl.textAlignment = NSTextAlignment.left
        employee1200Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee1200Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest1200 == guest1130 && employee1200 == employee1130)
//        {
//            guest1200Lbl.text = ""
//            employee1200Lbl.text = ""
//            view1200.backgroundColor = view1130.backgroundColor
//        }
//        else
//        {
            guest1200Lbl.text = guest1200
            employee1200Lbl.text = employee1200
            view1200.backgroundColor = UIColor.lightGray
//        }
        
        scrollview.addSubview(view1200)
        view1200.addSubview(time1200Lbl)
        view1200.addSubview(guest1200Lbl)
        view1200.addSubview(employee1200Lbl)
        
        // Time Slot for 12:30 PM to 1 PM
        let view1230 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/7), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time1230Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time1230Lbl.textAlignment = NSTextAlignment.left
        time1230Lbl.text = "- 12:30 PM -"
        guest1230Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest1230Lbl.textAlignment = NSTextAlignment.left
        employee1230Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee1230Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest1230 == guest1200 && employee1230 == employee1200)
//        {
//            guest1230Lbl.text = ""
//            employee1230Lbl.text = ""
//            view1230.backgroundColor = view1200.backgroundColor
//        }
//        else
//        {
            guest1230Lbl.text = guest1230
            employee1230Lbl.text = employee1230
            view1230.backgroundColor = UIColor.white
//        }
        
        scrollview.addSubview(view1230)
        view1230.addSubview(time1230Lbl)
        view1230.addSubview(guest1230Lbl)
        view1230.addSubview(employee1230Lbl)
        
        // Time Slot for 1:00 PM to 1:30 PM
        let view100 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/8), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time100Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time100Lbl.textAlignment = NSTextAlignment.left
        time100Lbl.text = "- 1:00 PM -"
        guest100Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest100Lbl.textAlignment = NSTextAlignment.left
        employee100Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee100Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest100 == guest1230 && employee100 == employee1230)
//        {
//            guest100Lbl.text = ""
//            employee100Lbl.text = ""
//            view100.backgroundColor = view1230.backgroundColor
//        }
//        else
//        {
            guest100Lbl.text = guest100
            employee100Lbl.text = employee100
            view100.backgroundColor = UIColor.lightGray
//        }
        
        scrollview.addSubview(view100)
        view100.addSubview(time100Lbl)
        view100.addSubview(guest100Lbl)
        view100.addSubview(employee100Lbl)
        
        // Time Slot for 1:30 PM to 2 PM
        let view130 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/9), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time130Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time130Lbl.textAlignment = NSTextAlignment.left
        time130Lbl.text = "- 1:30 PM -"
        guest130Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest130Lbl.textAlignment = NSTextAlignment.left
        employee130Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee130Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest130 == guest100 && employee130 == employee100)
//        {
//            guest130Lbl.text = ""
//            employee130Lbl.text = ""
//            view130.backgroundColor = view100.backgroundColor
//        }
//        else
//        {
            guest130Lbl.text = guest130
            employee130Lbl.text = employee130
            view130.backgroundColor = UIColor.white
//        }
        
        scrollview.addSubview(view130)
        view130.addSubview(time130Lbl)
        view130.addSubview(guest130Lbl)
        view130.addSubview(employee130Lbl)
        
        // Time Slot for 2:00 PM to 2:30 PM
        let view200 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/10), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time200Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time200Lbl.textAlignment = NSTextAlignment.left
        time200Lbl.text = "- 2:00 PM -"
        guest200Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest200Lbl.textAlignment = NSTextAlignment.left
        employee200Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee200Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest200 == guest130 && employee200 == employee130)
//        {
//            guest200Lbl.text = ""
//            employee200Lbl.text = ""
//            view200.backgroundColor = view130.backgroundColor
//        }
//        else
//        {
            guest200Lbl.text = guest200
            employee200Lbl.text = employee200
            view200.backgroundColor = UIColor.lightGray
//        }
        
        scrollview.addSubview(view200)
        view200.addSubview(time200Lbl)
        view200.addSubview(guest200Lbl)
        view200.addSubview(employee200Lbl)
        
        // Time Slot for 2:30 PM to 3 PM
        let view230 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/11), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time230Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time230Lbl.textAlignment = NSTextAlignment.left
        time230Lbl.text = "- 2:30 PM -"
        guest230Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest230Lbl.textAlignment = NSTextAlignment.left
        employee230Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee230Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest230 == guest200 && employee230 == employee200)
//        {
//            guest230Lbl.text = ""
//            employee230Lbl.text = ""
//            view230.backgroundColor = view200.backgroundColor
//        }
//        else
//        {
            guest230Lbl.text = guest230
            employee230Lbl.text = employee230
            view230.backgroundColor = UIColor.white
//        }
        
        scrollview.addSubview(view230)
        view230.addSubview(time230Lbl)
        view230.addSubview(guest230Lbl)
        view230.addSubview(employee230Lbl)
        
        // Time Slot for 3:00 PM to 3:30 PM
        let view300 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/12), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time300Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time300Lbl.textAlignment = NSTextAlignment.left
        time300Lbl.text = "- 3:00 PM -"
        guest300Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest300Lbl.textAlignment = NSTextAlignment.left
        employee300Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee300Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest300 == guest230 && employee300 == employee230)
//        {
//            guest300Lbl.text = ""
//            employee300Lbl.text = ""
//            view300.backgroundColor = view230.backgroundColor
//        }
//        else
//        {
            guest300Lbl.text = guest300
            employee300Lbl.text = employee300
            view300.backgroundColor = UIColor.lightGray
//        }
        
        scrollview.addSubview(view300)
        view300.addSubview(time300Lbl)
        view300.addSubview(guest300Lbl)
        view300.addSubview(employee300Lbl)
        
        // Time Slot for 3:30 PM to 4 PM
        let view330 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/13), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time330Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time330Lbl.textAlignment = NSTextAlignment.left
        time330Lbl.text = "- 3:30 PM -"
        guest330Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest330Lbl.textAlignment = NSTextAlignment.left
        employee330Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee330Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest330 == guest300 && employee330 == employee300)
//        {
//            guest330Lbl.text = ""
//            employee330Lbl.text = ""
//            view330.backgroundColor = view900.backgroundColor
//        }
//        else
//        {
            guest330Lbl.text = guest330
            employee330Lbl.text = employee330
            view330.backgroundColor = UIColor.white
//        }
        
        scrollview.addSubview(view330)
        view330.addSubview(time330Lbl)
        view330.addSubview(guest330Lbl)
        view330.addSubview(employee330Lbl)
        
        // Time Slot for 4:00 PM to 4:30 PM
        let view400 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/14), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time400Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time400Lbl.textAlignment = NSTextAlignment.left
        time400Lbl.text = "- 4:00 PM -"
        guest400Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest400Lbl.textAlignment = NSTextAlignment.left
        employee400Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee400Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest400 == guest330 && employee400 == employee330)
//        {
//            guest400Lbl.text = ""
//            employee400Lbl.text = ""
//            view400.backgroundColor = view330.backgroundColor
//        }
//        else
//        {
            guest400Lbl.text = guest400
            employee400Lbl.text = employee400
            view400.backgroundColor = UIColor.lightGray
//        }
        
        scrollview.addSubview(view400)
        view400.addSubview(time400Lbl)
        view400.addSubview(guest400Lbl)
        view400.addSubview(employee400Lbl)
        
        // Time Slot for 4:30 PM to 5:00 PM
        let view430 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/15), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time430Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time430Lbl.textAlignment = NSTextAlignment.left
        time430Lbl.text = "- 4:30 PM -"
        guest430Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest430Lbl.textAlignment = NSTextAlignment.left
        employee430Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee430Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest430 == guest400 && employee430 == employee400)
//        {
//            guest430Lbl.text = ""
//            employee430Lbl.text = ""
//            view430.backgroundColor = view400.backgroundColor
//        }
//        else
//        {
            guest430Lbl.text = guest430
            employee430Lbl.text = employee430
            view430.backgroundColor = UIColor.white
//        }
        
        scrollview.addSubview(view430)
        view430.addSubview(time430Lbl)
        view430.addSubview(guest430Lbl)
        view430.addSubview(employee430Lbl)
        
        // Time Slot for 5:00 PM to 5:30 PM
        let view500 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/16), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time500Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time500Lbl.textAlignment = NSTextAlignment.left
        time500Lbl.text = "- 5:00 PM -"
        guest500Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest500Lbl.textAlignment = NSTextAlignment.left
        employee500Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee500Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest500 == guest430 && employee500 == employee430)
//        {
//            guest500Lbl.text = ""
//            employee500Lbl.text = ""
//            view500.backgroundColor = view430.backgroundColor
//        }
//        else
//        {
            guest500Lbl.text = guest500
            employee500Lbl.text = employee500
            view500.backgroundColor = UIColor.lightGray
//        }
        
        scrollview.addSubview(view500)
        view500.addSubview(time500Lbl)
        view500.addSubview(guest500Lbl)
        view500.addSubview(employee500Lbl)
        
        // Time Slot for 5:30 PM to 6 PM
        let view530 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/17), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time530Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time530Lbl.textAlignment = NSTextAlignment.left
        time530Lbl.text = "- 5:30 PM -"
        guest530Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest530Lbl.textAlignment = NSTextAlignment.left
        employee530Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee530Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest530 == guest500 && employee530 == employee500)
//        {
//            guest530Lbl.text = ""
//            employee530Lbl.text = ""
//            view530.backgroundColor = view500.backgroundColor
//        }
//        else
//        {
            guest530Lbl.text = guest530
            employee530Lbl.text = employee530
            view530.backgroundColor = UIColor.white
//        }
        
        scrollview.addSubview(view530)
        view530.addSubview(time530Lbl)
        view530.addSubview(guest530Lbl)
        view530.addSubview(employee530Lbl)
        
        // Time Slot for 6:00 PM to 6:30 PM
        let view600 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/18), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time600Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time600Lbl.textAlignment = NSTextAlignment.left
        time600Lbl.text = "- 6:00 PM -"
        guest600Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest600Lbl.textAlignment = NSTextAlignment.left
        employee600Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee600Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest600 == guest530 && employee600 == employee530)
//        {
//            guest600Lbl.text = ""
//            employee600Lbl.text = ""
//            view600.backgroundColor = view530.backgroundColor
//        }
//        else
//        {
            guest600Lbl.text = guest600
            employee600Lbl.text = employee600
            view600.backgroundColor = UIColor.lightGray
//        }
        
        scrollview.addSubview(view600)
        view600.addSubview(time600Lbl)
        view600.addSubview(guest600Lbl)
        view600.addSubview(employee600Lbl)
        
        // Time Slot for 6:30 PM to 7 PM
        let view630 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/19), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time630Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time630Lbl.textAlignment = NSTextAlignment.left
        time630Lbl.text = "- 6:30 PM -"
        guest630Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest630Lbl.textAlignment = NSTextAlignment.left
        employee630Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee630Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest630 == guest600 && employee630 == employee600)
//        {
//            guest630Lbl.text = ""
//            employee630Lbl.text = ""
//            view630.backgroundColor = view600.backgroundColor
//        }
//        else
//        {
            guest630Lbl.text = guest630
            employee630Lbl.text = employee630
            view630.backgroundColor = UIColor.white
//        }
        
        scrollview.addSubview(view630)
        view630.addSubview(time630Lbl)
        view630.addSubview(guest630Lbl)
        view630.addSubview(employee630Lbl)
        
        // Time Slot for 7:00 PM to 7:30 PM
        let view700 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/20), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time700Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time700Lbl.textAlignment = NSTextAlignment.left
        time700Lbl.text = "- 7:00 PM -"
        guest700Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest700Lbl.textAlignment = NSTextAlignment.left
        employee700Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee700Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest700 == guest630 && employee700 == employee630)
//        {
//            guest700Lbl.text = ""
//            employee700Lbl.text = ""
//            view700.backgroundColor = view630.backgroundColor
//        }
//        else
//        {
            guest700Lbl.text = guest700
            employee700Lbl.text = employee700
            view700.backgroundColor = UIColor.lightGray
//        }
        
        scrollview.addSubview(view700)
        view700.addSubview(time700Lbl)
        view700.addSubview(guest700Lbl)
        view700.addSubview(employee700Lbl)
        
        // Time Slot for 7:30 PM to 8 PM
        let view730 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/21), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time730Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time730Lbl.textAlignment = NSTextAlignment.left
        time730Lbl.text = "- 7:30 PM -"
        guest730Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest730Lbl.textAlignment = NSTextAlignment.left
        employee730Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee730Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest730 == guest700 && employee730 == employee700)
//        {
//            guest730Lbl.text = ""
//            employee730Lbl.text = ""
//            view730.backgroundColor = view700.backgroundColor
//        }
//        else
//        {
            guest730Lbl.text = guest730
            employee730Lbl.text = employee730
            view730.backgroundColor = UIColor.white
//        }
        
        scrollview.addSubview(view730)
        view730.addSubview(time730Lbl)
        view730.addSubview(guest730Lbl)
        view730.addSubview(employee730Lbl)
        
        // Time Slot for 8:00 PM to 8:30 PM
        let view800 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/22), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time800Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time800Lbl.textAlignment = NSTextAlignment.left
        time800Lbl.text = "- 8:00 PM -"
        guest800Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest800Lbl.textAlignment = NSTextAlignment.left
        employee800Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee800Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest800 == guest730 && employee800 == employee730)
//        {
//            guest800Lbl.text = ""
//            employee800Lbl.text = ""
//            view800.backgroundColor = view730.backgroundColor
//        }
//        else
//        {
            guest800Lbl.text = guest800
            employee800Lbl.text = employee800
            view800.backgroundColor = UIColor.lightGray
//        }
        
        scrollview.addSubview(view800)
        view800.addSubview(time800Lbl)
        view800.addSubview(guest800Lbl)
        view800.addSubview(employee800Lbl)
        
        // Time Slot for 8:30 PM to 9 PM
        let view830 = UIView(frame: CGRect(x: 0, y: (self.view.frame.size.height * 6) / (24/23), width: self.view.bounds.size.width, height: (self.view.frame.size.height * 6) / 24))
        time830Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        time830Lbl.textAlignment = NSTextAlignment.left
        time830Lbl.text = "- 8:30 PM -"
        guest830Lbl = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.size.width, height: 45))
        guest830Lbl.textAlignment = NSTextAlignment.left
        employee830Lbl = UILabel(frame: CGRect(x: 10, y: 70, width: self.view.bounds.size.width, height: 45))
        employee830Lbl.textAlignment = NSTextAlignment.left
        
        //Check to see if it is the same Guest and Employee from previous time slot
//        if (guest830 == guest800 && employee830 == employee800)
//        {
//            guest830Lbl.text = ""
//            employee830Lbl.text = ""
//            view830.backgroundColor = view800.backgroundColor
//        }
//        else
//        {
            guest830Lbl.text = guest830
            employee830Lbl.text = employee830
            view830.backgroundColor = UIColor.white
//        }
        
        scrollview.addSubview(view830)
        view830.addSubview(time830Lbl)
        view830.addSubview(guest830Lbl)
        view830.addSubview(employee830Lbl)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
