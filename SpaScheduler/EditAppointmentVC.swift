//
//  EditAppointmentVC.swift
//  SpaScheduler
//
//  Created by Douglas Sass on 4/15/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit
import Parse

class EditAppointmentVC: UIViewController {

    var editAppointmentLbl = UILabel()
    
    var guestName = UITextField()
    var guestNameLbl = UILabel()
    
    var appointmentDatePicker = UIDatePicker()
    var appointmentDateLbl = UILabel()
    
    var appointmentTime = UITextField()
    var appointmentTimeLbl = UILabel()
    
    // var emailCreate = UITextField()
    var appointmentLengthLbl = UILabel()
    
    var appointmentRequestLbl = UILabel()
    
    var editAppointmentBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.white
        
        //Label for Add User Title
        editAppointmentLbl = UILabel(frame: CGRect(x: 10, y: 30, width: self.view.frame.size.width - 20, height: 45))
        editAppointmentLbl.text = "Edit the Appointment"
        editAppointmentLbl.textAlignment = NSTextAlignment.center
        editAppointmentLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(editAppointmentLbl)
        
        //Text field for the Guest's name for the appointment
        guestName = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        guestName.placeholder = "GuestName"
        guestName.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(guestName)
        
        // Label for the Guest's Name for the appointment
        guestNameLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        guestNameLbl.text = "Enter Guest's Name:"
        guestNameLbl.textAlignment = NSTextAlignment.center
        guestNameLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(guestNameLbl)
        
        // Date picker for the appointment
        appointmentDatePicker = UIDatePicker(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        self.view.addSubview(appointmentDatePicker)
        
        // Label for Date of appointment
        appointmentDateLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        appointmentDateLbl.text = "Date of Appointment:"
        appointmentDateLbl.textAlignment = NSTextAlignment.center
        appointmentDateLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentDateLbl)
        
        //Text field for the Time of the appointment
        appointmentTime = UITextField(frame: CGRect(x: 10, y: 160, width: self.view.frame.size.width - 20, height: 45))
        appointmentTime.placeholder = "Enter time in the Format 9:00 AM"
        appointmentTime.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentTime)
        
        //Label for Time
        appointmentTimeLbl = UILabel(frame: CGRect(x: 10, y: 130, width: self.view.frame.size.width - 20, height: 45))
        appointmentTimeLbl.text = "Time of Appointment:"
        appointmentTimeLbl.textAlignment = NSTextAlignment.center
        appointmentTimeLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentTimeLbl)
        
        //Decide if it is 30, 60, or 90 minute appointment Length
        //        emailCreate = UITextField(frame: CGRect(x: 10, y: 235, width: self.view.frame.size.width - 20, height: 45))
        //        emailCreate.placeholder = "email@email.com"
        //        emailCreate.autoresizingMask = UIViewAutoresizing.flexibleWidth
        //        self.view.addSubview(emailCreate)
        
        //Label for Length
        appointmentLengthLbl = UILabel(frame: CGRect(x: 10, y: 205, width: self.view.frame.size.width - 20, height: 45))
        appointmentLengthLbl.text = "Length of Appointment:"
        appointmentLengthLbl.textAlignment = NSTextAlignment.center
        appointmentLengthLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentLengthLbl)
        
        //Decide how to determine the Employee for request
        
        //Label for Requesting an Employee
        appointmentRequestLbl = UILabel(frame: CGRect(x: 10, y: 205, width: self.view.frame.size.width - 20, height: 45))
        appointmentRequestLbl.text = "Length of Appointment:"
        appointmentRequestLbl.textAlignment = NSTextAlignment.center
        appointmentRequestLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(appointmentRequestLbl)
        
        //Button to create user with username and password and email
        let editAppointmentBtn = UIButton(type: UIButtonType.system)
        editAppointmentBtn.frame = CGRect(x: 10, y: 300, width: self.view.frame.size.width - 20, height: 45)
        editAppointmentBtn.setTitle("Create Appointment", for: UIControlState.normal)
        editAppointmentBtn.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(editAppointmentBtn)
        editAppointmentBtn.addTarget(self, action: #selector(editAppointmentBtnTouched(btn:)), for: UIControlEvents.touchUpInside)
        
    }
    
    func editAppointmentBtnTouched(btn:UIButton) -> Void
    {
        // Need to verify the appointment time, length and employee are valid
        // If not valid, display error stating what is not allowed
        // If true, save information to serve and then push to the View Controller
        
        let obj = PFObject(className: "appointment")
        obj["aptGuestName"] = guestName.text
        obj["aptDate"] = appointmentDatePicker.date
        obj["aptTime"] = appointmentTime.text
        // obj["aptLength"] = appointmentLength.text
        // obj["aptRequest"] = appointmentRequest.text
        obj.saveInBackground { (success, error) in
            print("added")
        }
        
        // Push to the View Appointment view
        let vc = ViewAppointmentVC()
        vc.passedGuest = guestName.text!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy"
        let newDate = dateFormatter.string(from: appointmentDatePicker.date)
        vc.passedDate = newDate
        
        vc.passedTime = appointmentTime.text!
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
